import { Controller, Post } from "@nestjs/common";
import { AuthService } from "./auth.service";


@Controller('auth')
export class AuthController {
    // authService: AuthService;

    // constructor(authService: AuthService) {
    //     this.authService = authService;
    // }
    constructor(private authService: AuthService) {};

    @Post('signup')
    signup() {
        return this.authService.signup();
    }

    @Post('signin')
    signin() {
        return this.authService.login();        
    }
}