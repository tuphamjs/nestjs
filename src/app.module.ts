import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { BikeModule } from './bike/bike.module';
import { OrderModule } from './order/order.module';

@Module({
  imports: [AuthModule, UserModule, BikeModule, OrderModule],
})
export class AppModule {}